; Id

Panels Dashboard

Allows users to locally rearrange and roll up panes on the panel
and save state per account. Global state of panel is not modified
in the process.

DEPENDENCIES:
* Panels
* jQuery UI
If you're using non standard, custom layout template, please ensure that
.panel-display class is present on root div of your panel layout template.

INSTALLATION:
Download the module, place it in your modules directory and enable it in Drupal.
After doing so, Dashboard option should appear as available renderer under
General tab of panel variant you want to render as Dashboard. Tick this option
and enjoy!

After installing, you may want to tweak provided CSS (not very attractive)
to match your own theme.