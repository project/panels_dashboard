
/*
 * @file
 * Javascript file. Allows users to locally rearrange and roll up panes on the panel and save state per account
 */

/**
 * Snapshots current dashboard state to JSON
 */
function panelsDashboardToJSON() {
  var dashboardData = {}

  $(".dashboard-display-processed").each(function() {
    var dashboard = new Array();
    $(".dashboard-region").each(function() {
      var panes = new Array();
      $("#"+$(this).attr("id")+" .dashboard-pane").each(function() {
        panes.push("[\""+$(this).attr("id")+"\", "+($(this).hasClass("dashboard-rolled")?1:0)+"]");
      });
      dashboard.push("\""+$(this).attr("id")+"\":["+panes.join(",")+"]");
    });
    dashboardData["panels_dashboard_data"]= "{"+dashboard.join(",")+"}";
  });

  return dashboardData;
}

/**
 * Slides the pane up and sets correct class on it.
 */
function panelsDashboardSlideUp(pane, speed) {
  pane.addClass("dashboard-rolled");
  pane.children(":not(.dashboard-pane-handle)").slideUp(speed);
}

/**
 * Slides the pane down and removes class from it.
 */
function panelsDashboardSlideDown(pane, speed) {
  pane.removeClass("dashboard-rolled");
  pane.children(":not(.dashboard-pane-handle)").slideDown(speed);
}

/**
 * Updates dashboard after some actions
 */
function panelsDashboardRefresh() {
  $(".dashboard-display-processed").each(function() {
    // Sets or removes dashboard-region-empty classes based on wether the region is empty or not.
    $('.dashboard-region:not(:has(.dashboard-pane))').addClass("dashboard-region-empty");
    $('.dashboard-region:has(.dashboard-pane)').removeClass("dashboard-region-empty");
  });
}

/**
 * Saves dasboard's setup
 */
function panelsDashboardSave(data) {
  $.ajax({
    url: "/js/panels-dashboard/set/" + Drupal.settings.panelsDashboard.did,
    type: "POST",
    dataType: "json",
    data: data
  });
}

/**
 * Restores dasboard's setup
 */
function panelsDashboardRestore() {
  // Get current positions of panes and restore default setup
  $(".dashboard-display-processed").each(function() {
    $.ajax({
      url: "/js/panels-dashboard/get/" + Drupal.settings.panelsDashboard.did,
      dataType: "json",
      success: function(response) {
        if(response.status == "success" && response.data) {
          var data = response.data;
          for(slot in data) {
            for(pane in data[slot]) {
              // Restore pane to desired location
              $("#"+data[slot][pane][0]).appendTo("#"+slot);

              // Retore pane state (rolled up or not)
              if (data[slot][pane][1]) {
                panelsDashboardSlideUp($("#"+data[slot][pane][0]), 1);
              }
            }
          }
        }
        panelsDashboardRefresh();
        $(".dashboard-display-processed").show();
      }
    });
  });
}

Drupal.behaviors.panelsDashboard = function() {
  $(".panel-display:not(.dashboard-display-processed)").addClass('dashboard-display-processed').each(function() {
    // Hide the dashboard first. We don't want the user to see what's going on with the display during the rebuild process
    $(this).hide();

    // Remove elements that are not required or simply break the whole thing.
    $(".panel-region-separator").remove();
    $(".admin-links").remove();

    // Add handles to panes
    $(".dashboard-pane").each(function () {
      if($(this).children(".pane-title").length > 0) {
        $(".pane-title").addClass('dashboard-pane-handle');
      } else {
        $(this).prepend('<div class="dashboard-pane-handle"></div>');
      }
    });

    // Add roll-up, roll-down icons to each pane
    $(".dashboard-pane-handle").prepend('<div class="dashboard-roll-button"></div>');

    // Restore current setup
    panelsDashboardRestore();

    // Bind event to all roll up - roll down buttons
    $(".dashboard-roll-button").click(function() {
      if($(this).parents(".dashboard-pane").hasClass("dashboard-rolled")) {
        panelsDashboardSlideDown($(this).parents(".dashboard-pane"), 200);
      } else {
        panelsDashboardSlideUp($(this).parents(".dashboard-pane"), 200);
      }

      panelsDashboardSave(panelsDashboardToJSON());
    })

    // Bind sortables
    $(".dashboard-region:not(.dashboard-region-processed)").addClass('dashboard-region-processed').each(function() {
      $(this).sortable({
        placeholder: 'dashboard-placeholder-highlight',
        cursor: 'move',
        tolerance: 'pointer',
        handle: '.dashboard-pane-handle',
        revert: 200,
        delay: 100,
        start: function(event, ui) {          
          // If region contains only one pane, please set dashboard-region-empty class already.
          if ($(this).children(".dashboard-pane").length == 1) {
            $(this).addClass("dashboard-region-empty");
          }
        },
        stop: function(event, ui) {
          panelsDashboardRefresh();
          panelsDashboardSave(panelsDashboardToJSON());
        }
      });
    })

    $(".dashboard-region-processed").sortable("option", "connectWith", ['.dashboard-region']);
  });
}